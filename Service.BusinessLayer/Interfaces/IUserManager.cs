using System;
using System.Collections.Generic;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Interfaces
{
    public interface IUserManager
    {
        bool IsAccountLocked(string email);
        void UpdateLoginDetails(bool passwordValid, Guid guid, string ipAddress);
        Guid? Login(string email, string password, string ipAddress);
        bool IsEmailAddressAvailable(string email);
        Model.User CreateUser(string email, string firstName, string lastName, string password, List<Model.Role> roles, string currentUser);
        void DeleteUser(Guid id);
        void UpdateUser(Model.User userToUpdate, string currentUser);
        IEnumerable<Model.User> GetAllUsers();
        Model.User GetUserByEmail(string email);
        Model.User GetUserById(Guid id);
        Model.Role CreateRole(string name, List<Model.Permission> permissions, string description = null);
        void DeleteRole(Guid id);
        void UpdateRole(Model.Role role);
        IEnumerable<Model.Role> GetAllRoles();
        IEnumerable<Model.Role> GetUserRoles(Guid id);

        /// <summary>
        /// Gets the permissions associated with the given Role
        /// </summary>
        IEnumerable<Model.Permission> GetRolePermissions(Model.Role role);

        /// <summary>
        /// Gets the permissions associated with the given user based on the their roles
        /// </summary>
        IEnumerable<Model.Permission> GetUserPermissions(Model.User user);
    }
}