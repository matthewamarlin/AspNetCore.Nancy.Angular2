﻿using System;
using System.Runtime.Serialization;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Exceptions
{
    [Serializable]
    public class UserIdInUseExeption : Exception
    {
        public UserIdInUseExeption()
        { }

        public UserIdInUseExeption(string message) : base(message)
        { }

        public UserIdInUseExeption(string message, Exception innerException) : base(message, innerException)
        { }

        protected UserIdInUseExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}