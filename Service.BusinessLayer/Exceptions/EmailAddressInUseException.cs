﻿using System;
using System.Runtime.Serialization;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Exceptions
{
    [Serializable]
    public class EmailAddressInUseException : Exception
    {
        public EmailAddressInUseException()
        { }

        public EmailAddressInUseException(string message) : base(message)
        { }

        public EmailAddressInUseException(string message, Exception innerException) : base(message, innerException)
        { }

        protected EmailAddressInUseException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}