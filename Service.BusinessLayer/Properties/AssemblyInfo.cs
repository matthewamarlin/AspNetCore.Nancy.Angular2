﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer")]
[assembly: AssemblyDescription("A class library containing the core buisiness logic for the service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("9534b91a-7284-42b7-97f8-026ac055f02a")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]