﻿namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Model
{
    public sealed class Permission
    {
        public string Name { get; }
        public int Id { get; }

        public static readonly Permission AdministerSystem = new Permission(0, "AdministerSystem");
        public static readonly Permission AccountCreate = new Permission(1, "AccountCreate");
        public static readonly Permission AccountDelete = new Permission(2, "AccountDelete");
        public static readonly Permission AccountEdit = new Permission(3, "AccountEdit");
        public static readonly Permission AccountView = new Permission(4, "AccountView");
        public static readonly Permission ArtifactCreate = new Permission(5, "ArtifactCreate");
        public static readonly Permission ArtifactDelete = new Permission(6, "ArtifactDelete");
        public static readonly Permission ArtifactEdit = new Permission(7, "ArtifactEdit");
        public static readonly Permission ArtifactView = new Permission(8, "ArtifactView");
        public static readonly Permission AuditView = new Permission(9, "AuditView");
        public static readonly Permission FeedView = new Permission(10, "FeedView");
        public static readonly Permission ProjectCreate = new Permission(11, "ProjectCreate");
        public static readonly Permission ProjectDelete = new Permission(12, "ProjectDelete");
        public static readonly Permission ProjectEdit = new Permission(13, "ProjectEdit");
        public static readonly Permission ProjectView = new Permission(14, "ProjectView");
        public static readonly Permission ReleaseCreate = new Permission(15, "ReleaseCreate");
        public static readonly Permission ReleaseDelete = new Permission(16, "ReleaseDelete");
        public static readonly Permission ReleaseEdit = new Permission(17, "ReleaseEdit");
        public static readonly Permission ReleaseView = new Permission(18, "ReleaseView");
        public static readonly Permission UserRoleCreate = new Permission(19, "UserRoleCreate");
        public static readonly Permission UserRoleDelete = new Permission(20, "UserRoleDelete");
        public static readonly Permission UserRoleEdit = new Permission(21, "UserRoleEdit");
        public static readonly Permission UserRoleView = new Permission(22, "UserRoleView");
        public static readonly Permission UserView = new Permission(23, "UserView");

        public static implicit operator string(Permission permission) { return permission.Name; }
        public static implicit operator int(Permission permission) { return permission.Id; }
        
        private Permission(int id, string name)
        {
            Name = name;
            Id = id;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
