﻿using System;
using System.Collections.Generic;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Model
{
    public class User
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Hash { get; set; }
        public int FailedLogins { get; set; }
        public DateTime LastFailedLoginDate { get; set; }
        public string LastFailedLoginIpAddress { get; set; }
        public DateTime LastSuccessfulLoginDate { get; set; }
        public string LastSuccessfulLoginIpAddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public List<Guid> RoleIds { get; set; }
    }
}
