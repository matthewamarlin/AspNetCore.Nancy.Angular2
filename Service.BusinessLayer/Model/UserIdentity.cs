﻿using System;
using System.Collections.Generic;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Model
{
    public class UserIdentity
    {
        public Guid Guid { get; set; }
        public string UserName { get; set; }

        public IEnumerable<string> Claims { get; set; }
    }
}