﻿using System;
using System.Collections.Generic;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.BusinessLayer.Model
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> PermissionIds { get; set; }
    }
}
