﻿//using System;
//using System.CodeDom;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Linq;
//using AutoMapper;
//using SpearOne.AutoUpdate.DataAccess;
//using SpearOne.AutoUpdate.Service.BusinessLayer.Exceptions;
//using SpearOne.AutoUpdate.Service.BusinessLayer.Interfaces;
//using SpearOne.Common.Logging;
//using SpearOne.RepositoryPattern.EntityFramework6.Generic;

//namespace SpearOne.AutoUpdate.Service.BusinessLayer
//{
//    public class UserManager : IUserManager
//    {
//        private readonly ILogger _log;

//        private readonly int _maxFailedLogins;
//        private readonly int _lockoutDurationMins;

//        public UserManager(ILogger log)
//        {
//            _log = log;

//            _maxFailedLogins = Convert.ToInt32(ConfigurationManager.AppSettings["UserManager.MaxNumberOfLoginAttempts"]);
//            _lockoutDurationMins = Convert.ToInt32(ConfigurationManager.AppSettings["UserManager.LockoutDurationMins"]);

//            Mapper.Initialize(config =>
//            {
//                config.CreateMap<User, Model.User>()
//                      .ForMember(model => model.RoleIds, conf => conf.MapFrom(entity => entity.Roles.Select(x => x.Id).ToList()));
//                ;
//                config.CreateMap<Role, Model.Role>()
//                    .ForMember(model => model.PermissionIds, conf => conf.MapFrom(entity => entity.Permissions.Select(x => x.Id).ToList()));

//                config.CreateMap<Model.Role, Role>()
//                    .ForMember(entity => entity.Permissions, conf => conf.Ignore());

//                config.CreateMap<Model.User, Role>()
//                    .ForMember(entity => entity.Permissions, conf => conf.Ignore());

//                config.CreateMap<Permission, Model.Permission>();

//                config.CreateMap<Model.Permission, Permission>()
//                    .ForMember(entity => entity.Roles, conf => conf.Ignore());
//            });
//        }

//        #region Login

//        public bool IsAccountLocked(string email)
//        {
//            User user;

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);

//                user = userRepository.Get(x => x.Email == email).FirstOrDefault();
//            }

//            var lastFailureDate = user?.LastFailedLoginDate?.ToString();

//            // If there is no lockout timestamp then the account is not locked
//            if (lastFailureDate == null) return false;

//            var start = DateTime.Now;
//            var oldDate = DateTime.Parse(lastFailureDate);

//            var passed = start.Subtract(oldDate) >= TimeSpan.FromMinutes(_lockoutDurationMins);

//            return user.FailedLogins > _maxFailedLogins && !passed;
//        }

//        public void UpdateLoginDetails(bool passwordValid, Guid guid, string ipAddress)
//        {
//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);

//                var user = userRepository.Find(guid);
//                var captureDate = DateTime.Now;
//                var loginAttemptCount = user.FailedLogins;
//                var lastFailedLoginDate = user.LastFailedLoginDate;

//                // three login failures 
//                if (loginAttemptCount >= _maxFailedLogins)
//                {
//                    // check if the timespan has elapsed
//                    var start = DateTime.Now;
//                    var lastfail = DateTime.Parse(lastFailedLoginDate.ToString());

//                    if (start.Subtract(lastfail) >= TimeSpan.FromMinutes(_lockoutDurationMins))
//                    {
//                        // reset the counter
//                        user.FailedLogins = 0;
//                        user.LastFailedLoginIPAddress = ipAddress;

//                        userRepository.Update(user);
//                        return;
//                    }
//                }

//                if (passwordValid)
//                {
//                    user.LastSuccessfulLoginIPAddress = ipAddress;
//                    user.LastSuccessfulLoginDate = captureDate;

//                    userRepository.Update(user);
//                }
//                else
//                {
//                    // we increment the failed logins count here
//                    user.FailedLogins += 1;
//                    user.LastFailedLoginIPAddress = ipAddress;
//                    user.LastFailedLoginDate = captureDate;
//                }


//                unitOfWork.Commit();
//            }
//        }

//        public Guid? Login(string email, string password, string ipAddress)
//        {
//            // get the user row details from DB
//            var user = GetUserByEmail(email);

//            if (user == null) return null;

//            var doesPasswordMatch = BCrypt.Net.BCrypt.Verify(password, user.Hash);

//            UpdateLoginDetails(doesPasswordMatch, user.Id, ipAddress);

//            return doesPasswordMatch ? (Guid?)user.Id : null;
//        }

//        #endregion

//        #region Create

//        public bool IsEmailAddressAvailable(string email) => GetUserByEmail(email) == null;

//        #endregion

//        #region Users

//        public Model.User CreateUser(string email, string firstName, string lastName, string password, List<Model.Role> roles, string currentUser)
//        {
//            //TODO Improve Argument Handling
//            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentNullException(nameof(email));
//            if (string.IsNullOrWhiteSpace(firstName)) throw new ArgumentNullException(nameof(firstName));
//            if (string.IsNullOrWhiteSpace(lastName)) throw new ArgumentNullException(nameof(lastName));
//            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentNullException(nameof(password));
//            if (string.IsNullOrWhiteSpace(currentUser)) throw new ArgumentNullException(nameof(currentUser));
//            if (roles == null || !roles.Any()) throw new ArgumentException(nameof(roles));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);

//                var isEmailAddresssAvailable = !userRepository.Get(x => x.Email == email).Any();

//                if (!isEmailAddresssAvailable)
//                    throw new EmailAddressInUseException();

//                var userId = Guid.NewGuid();

//                var idsInUse = userRepository.GetAll().Select(x => x.Id).ToList();

//                while (idsInUse.Contains(userId))
//                {
//                    userId = Guid.NewGuid();
//                }

//                //TODO Verify this
//                var roleEntities = roleRepository.GetAll().Where(x => roles.Select(role => role.Id).Contains(x.Id)).ToList();

//                var now = DateTime.Now;

//                var userEntity = new User()
//                {
//                    Id = userId,
//                    Email = email,
//                    FirstName = firstName,
//                    LastName = lastName,
//                    Hash = BCrypt.Net.BCrypt.HashPassword(password),
//                    Roles = roleEntities,
//                    CreatedDate = now,
//                    LastUpdatedDate = now,
//                    LastUpdatedBy = currentUser
//                };

//                userRepository.Insert(userEntity);

//                unitOfWork.Commit();

//                return Mapper.Map<Model.User>(userEntity);
//            }
//        }

//        public void DeleteUser(Guid id)
//        {
//            if (id == null) throw new ArgumentNullException(nameof(id));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);
//                var user = userRepository.Find(id);

//                if (user == null) throw new UserNotFoundException();

//                //TODO Do you need to do this first?
//                user.Roles = new List<Role>();
//                unitOfWork.Commit();

//                userRepository.Delete(user);
//                unitOfWork.Commit();
//            }
//        }

//        public void UpdateUser(Model.User userToUpdate, string currentUser)
//        {
//            if (userToUpdate == null) throw new ArgumentNullException(nameof(userToUpdate));
//            if (userToUpdate.Id == null) throw new ArgumentException($"{nameof(userToUpdate.Id)} cannot be null");
//            if (string.IsNullOrWhiteSpace(currentUser)) throw new ArgumentNullException(nameof(currentUser));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);

//                var userFromDb = userRepository.Find(userToUpdate.Id);

//                if (userFromDb == null) throw new UserNotFoundException($"{nameof(User)} with {nameof(userToUpdate.Id)}, {userToUpdate.Id}, could not be found");

//                var updatedUser = Mapper.Map(userToUpdate, userFromDb);

//                //Due to our Mapping Config Roles wont be updated, so we do it manually
//                var rolesFromDb = new List<Role>();
//                foreach (var id in userToUpdate.RoleIds)
//                {
//                    var roleFromDb = roleRepository.Find(id);

//                    if (roleFromDb == null) throw new RoleNotFoundException($"Could not find a role with {nameof(id)}: {id}");

//                    rolesFromDb.Add(roleFromDb);
//                }

//                updatedUser.Roles = rolesFromDb;

//                updatedUser.LastUpdatedBy = currentUser;
//                updatedUser.LastUpdatedDate = DateTime.Now;

//                userRepository.Update(updatedUser);

//                unitOfWork.Commit();
//            }
//        }

//        public IEnumerable<Model.User> GetAllUsers()
//        {
//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);

//                var users = userRepository.GetAll().ToList().Select(Mapper.Map<Model.User>);

//                return users;
//            }
//        }

//        public Model.User GetUserByEmail(string email)
//        {
//            if (email == null) throw new ArgumentNullException(nameof(email));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);
//                return Mapper.Map<Model.User>(userRepository.Get(x => x.Email == email).FirstOrDefault());
//            }
//        }

//        public Model.User GetUserById(Guid id)
//        {
//            if (id == null) throw new ArgumentNullException(nameof(id));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);
//                return Mapper.Map<Model.User>(userRepository.Find(id));
//            }
//        }

//        #endregion

//        #region Roles

//        public Model.Role CreateRole(string name, List<Model.Permission> permissions, string description = null)
//        {
//            if (name == null) throw new ArgumentNullException(nameof(name));
//            if (permissions == null) throw new ArgumentNullException(nameof(permissions));
//            if (permissions.Count < 1) throw new ArgumentException($"A {nameof(Role)} requires at least one {nameof(Permission)}");

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);
//                var permissionRepository = new Repository<AutoUpdateEntities, Permission>(unitOfWork);

//                var roleId = Guid.NewGuid();
//                var idsInUse = roleRepository.GetAll().Select(x => x.Id).ToList();

//                while (idsInUse.Contains(roleId))
//                {
//                    roleId = Guid.NewGuid();
//                }

//                var permissionsFromDb = new List<Permission>();
//                foreach (var id in permissions.Select(x => x.Id))
//                {
//                    var permissionFromDb = permissionRepository.Find(id);

//                    if (permissionFromDb == null) throw new PermissionNotFoundException($"Could not find a permission with {nameof(id)}: {id}");

//                    permissionsFromDb.Add(permissionFromDb);
//                }

//                var roleEntity = new Role()
//                {
//                    Id = roleId,
//                    Name = name,
//                    Description = description,
//                    Permissions = permissionsFromDb
//                };

//                roleRepository.Insert(roleEntity);

//                unitOfWork.Commit();

//                return Mapper.Map<Model.Role>(roleEntity);
//            }
//        }

//        public void DeleteRole(Guid id)
//        {
//            if (id == null) throw new ArgumentNullException(nameof(id));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);
//                var role = roleRepository.Find(id);

//                if (role == null) throw new RoleNotFoundException();

//                //TODO Do you need to do this first?
//                role.Permissions = new List<Permission>();
//                unitOfWork.Commit();

//                roleRepository.Delete(id);
//                unitOfWork.Commit();
//            }
//        }

//        public void UpdateRole(Model.Role role)
//        {
//            if (role == null) throw new ArgumentNullException(nameof(role));
//            if (role.Id == null) throw new ArgumentException($"{nameof(role.Id)} cannot be null");

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);
//                var permissionRepository = new Repository<AutoUpdateEntities, Permission>(unitOfWork);

//                var roleFromDb = roleRepository.Find(role.Id);

//                if (roleFromDb == null) throw new RoleNotFoundException($"{nameof(User)} with {nameof(role.Id)}, {role.Id}, could not be found");

//                var updatedRole = Mapper.Map(role, roleFromDb);

//                //Due to our Mapping Config Roles wont be updated, so we do it manually
//                var permissionsFromDb = new List<Permission>();
//                foreach (var id in role.PermissionIds)
//                {
//                    var permissionFromDb = permissionRepository.Find(id);

//                    if (permissionFromDb == null) throw new PermissionNotFoundException($"Could not find a {nameof(Permission)} with {nameof(id)}: {id}");

//                    permissionsFromDb.Add(permissionFromDb);
//                }

//                updatedRole.Permissions = permissionsFromDb;

//                roleRepository.Update(updatedRole);

//                unitOfWork.Commit();
//            }
//        }

//        public IEnumerable<Model.Role> GetAllRoles()
//        {
//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);

//                return roleRepository.GetAll().Select(Mapper.Map<Model.Role>).ToList();
//            }
//        }

//        public IEnumerable<Model.Role> GetUserRoles(Guid id)
//        {
//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);

//                return userRepository.Find(id)?.Roles.Select(Mapper.Map<Model.Role>);
//            }
//        }

//        #endregion

//        #region Permissions

//        /// <summary>
//        /// Gets the permissions associated with the given Role
//        /// </summary>
//        public IEnumerable<Model.Permission> GetRolePermissions(Model.Role role)
//        {
//            if (role == null) throw new ArgumentNullException(nameof(role));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var roleRepository = new Repository<AutoUpdateEntities, Role>(unitOfWork);

//                var roleFromDb = roleRepository.Find(role.Id);

//                return roleFromDb.Permissions.ToList().Select(Mapper.Map<Model.Permission>);
//            }
//        }

//        /// <summary>
//        /// Gets the permissions associated with the given user based on the their roles
//        /// </summary>
//        public IEnumerable<Model.Permission> GetUserPermissions(Model.User user)
//        {
//            if (user == null) throw new ArgumentNullException(nameof(user));

//            using (var unitOfWork = new UnitOfWork<AutoUpdateEntities>())
//            {
//                var userRepository = new Repository<AutoUpdateEntities, User>(unitOfWork);

//                var userFromDb = userRepository.Find(user.Id);
//                var permissions = userFromDb.Roles.SelectMany(x => x.Permissions);

//                return permissions.ToList().Select(Mapper.Map<Model.Permission>);
//            }
//        }

//        #endregion
//    }
//}