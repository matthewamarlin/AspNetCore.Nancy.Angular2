using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.Nancy.Angular2.Tests.Unit")]
[assembly: AssemblyDescription("This assembly contains all the Unit test logic")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("30d63032-5ee8-4b92-85a0-7eb5ee2a4573")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]