﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Proxy")]
[assembly: AssemblyDescription("A class library containing a proxy implementation for interacting with the 'Nancy.Angular2' service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("8c437c02-bc72-42d9-9cde-f5501010cb4b")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]