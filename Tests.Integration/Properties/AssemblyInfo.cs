using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.Nancy.Angular2.Tests.Integration")]
[assembly: AssemblyDescription("This assembly contains all the Integration test logic")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("31205078-a3e0-4aa3-b2ae-4250e896aa07")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]