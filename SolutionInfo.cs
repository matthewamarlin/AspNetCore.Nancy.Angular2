﻿using System.Reflection;

[assembly: AssemblyCompany("SpearOne")]
[assembly: AssemblyProduct("SpearOne.Examples.Nancy.Angular2")]
[assembly: AssemblyCopyright("Copyright © SpearOne 2017")]
[assembly: AssemblyTrademark("Tramemark ™ SpearOne 2017")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyInformationalVersion("1.0.0")]