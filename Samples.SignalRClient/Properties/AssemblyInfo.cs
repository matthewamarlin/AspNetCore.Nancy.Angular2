﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.Nancy.Angular2.Samples.SignalRClient")]
[assembly: AssemblyDescription("This is an example TestClient using the SignalR protocol")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("8f752559-ba17-4b29-b653-47f179bacc18")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]