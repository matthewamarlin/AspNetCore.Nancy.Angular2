using Microsoft.AspNetCore.Mvc;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Website.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
