﻿$(function () {

    var connection = $.hubConnection("http://localhost:4505/signalr");
    var contosoChatHubProxy = connection.createHubProxy("chatHub");

    contosoChatHubProxy.on("receiveMessage", function (message) {
        console.log(message);
    });

    connection.received(function (data) {
        //console.log(data);
    });

    connection.error(function (error) {
        console.warn(error);
    });

    connection.stateChanged(function (change) {
        if (change.newState === $.signalR.connectionState.reconnecting) {
            console.log("Re-connecting");
        }
        else if (change.newState === $.signalR.connectionState.connected) {
            console.log("The server is online");
        }
    });

    connection.reconnected(function () {
        console.log("Reconnected");
    });

    $(document).ready(function () {
        connection.start().done(function () {
            console.log("Connection started!");
        });
    });
})