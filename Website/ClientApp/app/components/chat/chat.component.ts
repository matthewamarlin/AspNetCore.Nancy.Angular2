import { Component, OnInit, Input } from "@angular/core";
import { Http, Response } from "@angular/http";
import { ChatService } from "../../services/chat.service";

@Component({
    selector: 'chat',
    templateUrl: './chat.component.html'
})
export class ChatComponent implements OnInit {
    @Input() apiUrl: string;

    messages: string = "...Welcome...";

    constructor(
        private http: Http,
        private chatService: ChatService
    ) {

    }

    ngOnInit() {
        
        this.chatService.messageReceived$.subscribe(
            (message: string) => {
                this.writeMessage(message);
            },
            (error: any) => {
                console.warn("Error subscribing to messageReceived$ Event: ", error); //TODO Does this work?
            }
        );

        this.chatService.start();
    }


    private writeMessage(message: string): void {
        this.messages += (`\n${message}`);
        console.log(message);
    }

    callApi() {
        this.http.get(this.apiUrl)
            .map((res: Response) => res)
            .subscribe((res) => { console.log(res); });
    }
}
