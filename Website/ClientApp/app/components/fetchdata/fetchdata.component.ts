import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'fetchdata',
    templateUrl: './fetchdata.component.html'
})
export class FetchDataComponent {
    public forecasts: WeatherForecast[];

    constructor(http: Http, @Inject('ORIGIN_URL') originUrl: string, @Inject('SERVICE_URL') serviceUrl: string) {
        http.get(serviceUrl + '/api/test/sample-data').subscribe(result => {
            this.forecasts = JSON.parse(result.text()) as WeatherForecast[];
        });
    }
}

interface WeatherForecast {
    DateFormatted: string;
    TemperatureC: number;
    TemperatureF: number;
    Summary: string;
}
