import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { sharedConfig } from './app.module.shared';


import 'rxjs/add/operator/map';
import { ChatComponent } from './components/chat/chat.component';
import { ChatService, SignalrWindow  } from './services/chat.service';

@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: [
        ChatComponent,
        ...sharedConfig.declarations
    ],
    imports: [
        RouterModule.forRoot([
            { path: 'chat', component: ChatComponent },
        ]),
        BrowserModule,
        FormsModule,
        HttpModule,
        ...sharedConfig.imports
    ],
    providers: [
        { provide: 'ORIGIN_URL', useValue: location.origin },

        //SignalR Providers
        ChatService,
        { provide: SignalrWindow, useValue: window },
        ...sharedConfig.providers
    ]
})
export class AppModule {

}
