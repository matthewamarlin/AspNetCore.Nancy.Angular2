# AspNetCore.Nancy Angular2

This is an example/template solution for a *Single Page Application (SPA)* written using [Angular2] and hosted with [ASP.NET Core] (Mostly just for the hosting capabilities within a service container and the debugging functioinality in VS). This solution also contains a server side API written using [Nancy] and [SignalR] which runs on the standard ./Net Framework - these will be ported to .Net Core once the libraries are more mature.

## How to get started

Provide code examples and explanations of how to get the project.

### Prerequisites

- [Visual Studio 2017 Comminuty][Visual Studio Downloads]
- [NodeJS] - The version used as of publishing this is [node-v7.10.0-x64]
- [NPM] - `npm` is a package manager for JavaScript - this is installed along with node  
- [.NET Core] - Microsofts .Net Core Currently at v1.1

### Recommended Tools / Plugins
- [WebEssentials 2017] Visual Studio Plugin

## License

This project is under the [MIT License]

## Still to be done

- Implement Stateless Authentication
- Implement User Authorization / Authentication
- Improve Document Generation
- Create Confluence Documentation Page

-----------------------------------------
[Mit License]: https://opensource.org/licenses/MIT
[NodeJS]: https://nodejs.org/en/
[Visual Studio Downloads]: https://www.visualstudio.com/downloads/
[node-v7.10.0-x64]: https://nodejs.org/dist/v7.10.0/node-v7.10.0-x64.msi
[NPM]: https://www.npmjs.com/
[WebEssentials 2017]: https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebExtensionPack2017
[.NET Core]: https://www.microsoft.com/net/download/core
[ASP.NET Core]: https://docs.microsoft.com/en-us/aspnet/core/
[WebPack]: https://webpack.js.org/
[Angular2]: https://angular.io
[LESS]: http://lesscss.org/
[TypeScript]: https://www.typescriptlang.org/
[Nancy]: http://nancyfx.org/
[SignalR]: https://www.asp.net/signalr