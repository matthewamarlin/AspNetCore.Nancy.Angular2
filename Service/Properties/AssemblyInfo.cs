﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.AspNetCore.Nancy.Angular2.Service")]
[assembly: AssemblyDescription("The core service logic for the Nancy.Angular2 service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("5bc4d2c9-7d4d-4521-bdee-2494d8a4f29c")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]