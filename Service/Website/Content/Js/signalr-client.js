﻿$(function () {

    var connection = $.hubConnection("http://spearone.net/Nancy-Angular2/api");
    var contosoChatHubProxy = connection.createHubProxy("managementHub");

    contosoChatHubProxy.on("recieveOutput", function (data) {
        console.log(data);
    });

    connection.received(function (data) {
        console.log(data);
    });

    connection.error(function (error) {
        console.warn(error);
    });

    connection.stateChanged(function (change) {
        if (change.newState === $.signalR.connectionState.reconnecting) {
            console.log("Re-connecting");
        }
        else if (change.newState === $.signalR.connectionState.connected) {
            console.log("The server is online");
        }
    });

    connection.reconnected(function () {
        console.log("Reconnected");
    });

    $(document).ready(function () {
        connection.start().done(function () {
            console.log("Connection started!");
        });
    });
})