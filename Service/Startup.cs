﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Cors;
using Owin;
using SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Composition;
using SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Composition.SignalR;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service
{
    public class Startup
    {
        public Startup()
        {
            DependencyResolver.Initialize();
        }

        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            app.Map("/signalr", builder => {
                builder.MapSignalR();
            });

            app.Map("", builder => {
                builder.UseNancy();
            });

            var activator = new SimpleInjectorHubActivator();
            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => activator);
        }
    }
}