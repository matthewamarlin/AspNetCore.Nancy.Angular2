﻿using Microsoft.AspNet.SignalR;
using SpearOne.Common.Logging;
using SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Contracts;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Hubs
{
    public class ChatHub : Hub<IChatHubClient>, IChatHub
    {
        private readonly ILogger _log;

        public ChatHub(ILogger log)
        {
            _log = log;
        }

        public void Send(string message)
        {
            _log.Write(Level.Debug, $"{nameof(Send)}: {message}");
            Clients.Others.ReceiveMessage(message);
        }
    }
}