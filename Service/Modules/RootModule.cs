using Nancy;
using Nancy.Responses;
using Nancy.Routing;
using SpearOne.Common.Logging;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Modules
{
    public class RootModule : NancyModule
    {
        public RootModule(IRouteCacheProvider routeCacheProvider, ILogger log)
        {
            log.Write(Level.Trace, "Initialising RootModule");

            Get["/"] = routeData => new RedirectResponse(Request.Url + "/api/");
            Get["/api/"] = routeData => View["Routes", routeCacheProvider.GetCache()];
        }
    }
}