﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Nancy;
using Newtonsoft.Json;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Modules
{
    public class TestModule : NancyModule
    {
        public TestModule()
        {
            Get["/api/test/server-time"] = routeData => JsonConvert.SerializeObject(DateTime.Now.ToString(CultureInfo.InvariantCulture));
            Get["/api/test/sample-data"] = routeData => JsonConvert.SerializeObject(WeatherForecasts());
        }

        private static readonly string[] Summaries = {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }
            public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
        }
    }
}