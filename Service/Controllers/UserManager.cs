﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Nancy;
//using Nancy.Authentication.Forms;
//using Nancy.Security;
//using SpearOne.AutoUpdate.Service.Models;
//using SpearOne.Common.Logging;

//namespace SpearOne.AutoUpdate.Service.Controllers
//{

//    public class UserManager : BusinessLayer.UserManager, IUserMapper 
//    {
//        public UserManager(ILogger log) : base(log)
//        { }

//        public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
//        {
//            var user = GetUserById(identifier);
            
//            // create a simple string array and loop the list of roles and add them to it.
//            var claims = GetUserPermissions(user).Select(x => x.Name);

//            return user == null
//                ? null
//                : new NancyUserIdentity
//                {
//                    UserName = user.Email,
//                    Guid = user.Id,
//                    Claims = claims
//                };
//        }
//    }
//}