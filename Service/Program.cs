﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Owin.Hosting;
using Nancy;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service
{
    public class Program
    {
        private readonly ILogger _log;

        public Program()
        {
            _log = new Log4NetWrapper(typeof(Program));

            SetConsoleCtrlHandler(OnConsoleExitHandler, true);

            // Add the hostnames you want
            var options = new StartOptions();
            options.Urls.Add("http://localhost:4505/");
            options.Urls.Add("http://spearone.net:4505/");

            // Start SignalR &Nancy
            using (WebApp.Start(options))
            {
                try
                {
                    if(StaticConfiguration.IsRunningDebug) Process.Start($"{options.Urls[0]}");

                    _log.Write(Level.Info, $"{typeof(Program).Namespace} Started");
                    _log.Write(Level.Info, $"Server Bindings: \n -> {string.Join(Environment.NewLine +" -> ", options.Urls)}");

                    Console.ReadLine();
                }
                catch (Exception exception)
                {
                    _log.Write(Level.Fatal, exception);
                }
                finally
                {
                    //Do something?
                }
            }
        }

        #region Console Close CtrlEventHandler

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(CtrlEventHandler handler, bool add);

        public delegate bool CtrlEventHandler(CtrlType sig);

        public enum CtrlType
        {
            CtrlCEvent = 0,
            CtrlBreakEvent = 1,
            CtrlCloseEvent = 2,
            CtrlLogoffEvent = 5,
            CtrlShutdownEvent = 6
        }

        private static bool OnConsoleExitHandler(CtrlType sig)
        {
            Console.WriteLine("Exiting system due to external CTRL-C, or process kill, or shutdown");

            //TODO DO Cleanup

            Console.WriteLine("Cleanup complete");

            // Shutdown right away so there are no lingering threads
            Environment.Exit(-1);

            return true;
        }

        #endregion

        private static void Main(string[] args)
        {
            var program = new Program();
        }
    }
}
