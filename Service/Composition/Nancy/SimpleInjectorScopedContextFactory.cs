﻿using Nancy;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Composition.Nancy
{
    public sealed class SimpleInjectorScopedContextFactory : INancyContextFactory
    {
        private readonly Container _container;
        private readonly INancyContextFactory _defaultFactory;

        public SimpleInjectorScopedContextFactory(Container container, INancyContextFactory @default)
        {
            _container = container;
            _defaultFactory = @default;
        }

        public NancyContext Create(Request request)
        {
            var context = _defaultFactory.Create(request);
            context.Items.Add("SimpleInjector.Scope", AsyncScopedLifestyle.BeginScope(_container));
            return context;
        }
    }
}
