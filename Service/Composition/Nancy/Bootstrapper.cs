﻿using System;
using System.IO;
using log4net.Config;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.Routing;
using Nancy.TinyIoc;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

//using Nancy.Authentication.Forms;
//using Nancy.Cryptography;
//using SpearOne.AutoUpdate.Service.BusinessLayer.Interfaces;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Composition.Nancy
{
    public sealed class Bootstrapper : DefaultNancyBootstrapper
    {
        //private readonly CryptographyConfiguration _cryptographyConfiguration;
        private readonly Container _container = new Container();

        public Bootstrapper()
        {
            InitLog4Net();
            InitCryptographicSettings();
            InitNancyConventions();
        }

        private void InitLog4Net()
        {
            // Configure Log4Net
            XmlConfigurator.Configure();
        }

        private void InitNancyConventions()
        {
            // Add a view location convention that looks for views in the Website folder
            Conventions.ViewLocationConventions.Add((viewName, model, context) => $"../Views/{viewName}");
            

            // Add a new path for static content so our typescript files located in
            //  the 'App' folder can be served to SystemJS
            Conventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("dist"));
            Conventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Website")));
        }

        private void InitCryptographicSettings()
        {
            // Create the cryptography config
            //_cryptographyConfiguration = new CryptographyConfiguration(
            //           new RijndaelEncryptionProvider(new PassphraseKeyGenerator("SuperSecretPass", new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 })),
            //           new DefaultHmacProvider(new PassphraseKeyGenerator("UberSuperSecure", new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 })));
        }

        private void InitSimpleInjectorContainer(TinyIoCContainer nancysContainer)
        {
            // Create Simple Injector container
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            // Register application components here:

            // This registers an ILogger such that when a caller requires it for example a class named 'ServiceA', it will be give the implementation 'Log4NetWrapper(typeof(ServiceA))'
            // This is done so that the Log4NetWrapper knows the details of the calling class and can give contextual messages - I.E class specific log calls.
            _container.RegisterConditional(typeof(ILogger),
                x => typeof(Log4NetWrapper<>).MakeGenericType(x.Consumer.ImplementationType),
                Lifestyle.Singleton,
                x => true);

            // Register Nancy modules.
            foreach (var nancyModule in Modules)
            {
                _container.Register(nancyModule.ModuleType);
            }

            _container.Register<IRouteCacheProvider>(nancysContainer.Resolve<IRouteCacheProvider>, Lifestyle.Transient);
            
            //_container.Register<IUserManager, UserManager>(Lifestyle.Singleton);
            //_container.Register<IUserMapper, UserManager>(Lifestyle.Singleton);

            // Check the container is valid and required types have been registrered.
            _container.Verify();
        }

        protected override void ApplicationStartup(TinyIoCContainer nancysContainer, IPipelines pipelines)
        {
            InitSimpleInjectorContainer(nancysContainer);

            // Hook up Simple Injector in the Nancy pipeline.
            nancysContainer.Register(typeof(INancyModuleCatalog), new SimpleInjectorModuleCatalog(_container));
            nancysContainer.Register(typeof(INancyContextFactory), new SimpleInjectorScopedContextFactory(_container, nancysContainer.Resolve<INancyContextFactory>()));
        }

        protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context)
        {
            // At request startup we modify the request pipelines to
            // include forms authentication - passing in our now request
            // scoped user name mapper.
            //
            //var formsAuthConfiguration =
            //    new FormsAuthenticationConfiguration
            //    {
            //        RedirectUrl = "~/login",
            //        UserMapper = _container.GetInstance<IUserMapper>(),
            //        CryptographyConfiguration = _cryptographyConfiguration
            //    };

            //FormsAuthentication.Enable(pipelines, formsAuthConfiguration);
        }

        protected override IRootPathProvider RootPathProvider => new CustomRootPathProvider();
    }

    public class CustomRootPathProvider : IRootPathProvider
    {
        public string GetRootPath()
        {
            //return StaticConfiguration.IsRunningDebug
            //    ? Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..", ".."))
            //    : AppDomain.CurrentDomain.BaseDirectory;

            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Website");
        }
    }
}