﻿using System;
using System.ServiceModel;
using log4net.Config;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

//using SimpleInjector.Lifestyles;
//using SpearOne.AutoUpdate.Service.BusinessLayer.Interfaces;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Composition
{
    public class DependencyResolver
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(DependencyResolver));

        private const string ContainerNotSetMessage = "The dependency injection container has not been initialised. Please " +
                                                      "call the Initilize method on the ServiceCompositionRoot before requesting any types.";

        private static Container _container;

        public static void Initialize()
        {
            try
            {
                XmlConfigurator.Configure();

                Log.Write(Level.Debug, $"{nameof(DependencyResolver)} Initialising");
                
                _container = new Container();
                _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

                //Register Logging Framework
                _container.RegisterConditional(typeof(ILogger),
                                   x => typeof(Log4NetWrapper<>).MakeGenericType(x.Consumer.ImplementationType),
                                   Lifestyle.Singleton,
                                   x => true);

                //Nancy Module Registrations
                //foreach (var nancyModule in Assembly.GetExecutingAssembly().GetTypes().Where(type => type.)) _container.Register(nancyModule.ModuleType);
                //_container.RegisterCollection(typeof(NancyModule), Assembly.GetExecutingAssembly());

                //Business Layer Registrations
                //_container.Register<IUserManager, UserManager>();


                //Finalization
                _container.Verify();
            }
            catch (Exception exception)
            {
                Log.WriteFormat(Level.Error, "An error occured when attempting to initialise the service: {0}", exception.Message);
                throw new FaultException($"Service Configuration Error: {exception.Message} ");
            }

        }

        public static T GetInstance<T>() where T : class
        {
            if (_container == null)
                throw new InvalidOperationException(ContainerNotSetMessage);

            return _container.GetInstance<T>();
        }

        public static object GetInstance(Type serviceType)
        {
            if (_container == null)
                throw new InvalidOperationException(ContainerNotSetMessage);

            return _container.GetInstance(serviceType);
        }

        //public InstanceProducer[] GetCurrentRegistrations() => _container.GetCurrentRegistrations();
    }
}
