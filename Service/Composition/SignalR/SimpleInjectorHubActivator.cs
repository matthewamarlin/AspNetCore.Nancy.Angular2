﻿using Microsoft.AspNet.SignalR.Hubs;

namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Composition.SignalR
{
    public class SimpleInjectorHubActivator : IHubActivator
    {
        public IHub Create(HubDescriptor descriptor)
        {           
            return (IHub) DependencyResolver.GetInstance(descriptor.HubType);
        }
    }
}
