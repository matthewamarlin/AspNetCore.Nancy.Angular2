﻿namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Contracts
{
    public interface IChatHubClient
    {
        void ReceiveMessage(string data);
    }
}
