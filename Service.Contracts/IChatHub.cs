﻿namespace SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Contracts
{
    public interface IChatHub
    {
        void Send(string message);
    }
}
