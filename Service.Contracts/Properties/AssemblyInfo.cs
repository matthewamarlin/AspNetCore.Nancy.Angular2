﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.AspNetCore.Nancy.Angular2.Service.Contracts")]
[assembly: AssemblyDescription("A class library containing the common Types used when communicating with the Nancy.Angular2 service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("f0025f5d-4539-498a-a1fa-c5383cf41a16")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]